#include <stdlib.h>
#include <stdio.h>

#include <beest/file.h>
#include <beest/output.h>
#include <beest/process.h>

/* ----- */

#define CWDBUF 1024

/* ----- */

void main01()
{
    ret_t ret;
    char const cwd[CWDBUF];
    outstr_t out = OUT_STR(cwd, CWDBUF);
    proc_cmd_t cmd;

    ret.err = file_getcwd(&out);
    if (ret.err == ERR_ANY) { abort(); }
    printf("cwd: %s\n=====\n", cwd);

    ret.err = file_chdir(cwd);
    if (ret.err == ERR_ANY) { abort(); }

    cmd.command = "ls -l . notfound";
    ret = proc_exec_shell(&out, &cmd);
    if (ret.err == ERR_ANY) { abort(); }
    printf("%s:\n%s=====\n", cmd.command, cwd);

    ret.err = file_copy("build/file.o", "build/file.copy.o");
    if (ret.err == ERR_ANY) { abort(); }
    ret.err = file_rename("build/file.copy.o", "build/file.o");
    if (ret.err == ERR_ANY) { abort(); }

    // glob
}
