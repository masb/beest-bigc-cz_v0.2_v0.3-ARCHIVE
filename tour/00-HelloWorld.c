#include <stdlib.h>
#include <stdio.h>

#include <beest/stdio.h>

/* ----- */

void main00()
{
    uret32_t ret = io_write("hello, world");
    if (ret.err == ERR_ANY) { abort(); }
    printf(": %d bytes written\n=====\n", ret.val);
}
